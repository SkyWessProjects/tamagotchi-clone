﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionButton : MonoBehaviour
{
    public Critter critter;
    public CritterInteraction interaction;

    public void onButtonClick()
    {
        critter.interact(interaction);
    }
    
}
