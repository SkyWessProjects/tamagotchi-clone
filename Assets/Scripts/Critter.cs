﻿using UnityEngine;
using UnityEngine.UI;

public enum CritterInteraction
{
    play,
    feed,
    ignore,
    sleep
}
public class Critter : MonoBehaviour
{
    public int boredom = 0;
    public int hunger = 0;
    public int energy = 0;

    public float healthDTime = 10f;
    public float growthProgress = 0f;

    public Sprite happyImage;
    public Sprite goodImage;
    public Sprite neutralImage;
    public Sprite stressedImage;
    public Sprite deadImage;

    public Sprite happyImage2;
    public Sprite goodImage2;
    public Sprite neutralImage2;
    public Sprite stressedImage2;
    public Sprite deadImage2;

    public Sprite happyImage3;
    public Sprite goodImage3;
    public Sprite neutralImage3;
    public Sprite stressedImage3;
    public Sprite deadImage3;

    public Image portrait;
    public GameObject resetButton;
    public Text stressText;

    private int levelCounter = 0;
    public void interact(CritterInteraction interaction)
    {
        if(interaction == CritterInteraction.play && boredom >= 0)
        {
            boredom -= 3;
        }
        else if (interaction == CritterInteraction.feed && hunger >= 0)
        {
            hunger -= 3;
        }
        else if (interaction == CritterInteraction.ignore)
        {
            boredom += 1;
            hunger += 1;
            energy -= 1;
        }
        else if (interaction == CritterInteraction.sleep && energy <= 0)
        {
            energy += 3;
        }
        portrait.sprite = getCurrentMood();
    }
    public Sprite getCurrentMood()
    {
        int stress = hunger + boredom - energy;

        if(stress < 9)
        {
            return happyImage;
        }
        else if (stress < 18)
        {
            return goodImage;
        }
        else if (stress < 27)
        {
            return neutralImage;
        }
        else if (stress < 36)
        {
            return stressedImage;
        }
        else
        {
            resetButton.SetActive(true);
            return deadImage;
        }
    }
    private void Update()
    {
        int stress = hunger + boredom - energy;
        stressText.text = "Stress: " + stress;

        if(healthDTime > 0)
        {
            healthDTime -= Time.deltaTime;
            growthProgress += (stress * Time.deltaTime) / 2;
            if(growthProgress >= 500 && levelCounter == 0)
            {
                happyImage = happyImage2;
                goodImage = goodImage2;
                neutralImage = neutralImage2;
                stressedImage = stressedImage2;
                deadImage = deadImage2;
                growthProgress = 0f;

                portrait.sprite = getCurrentMood();
                levelCounter++;
            }
            else if (growthProgress >= 1000 && levelCounter == 1)
            {
                happyImage = happyImage3;
                goodImage = goodImage3;
                neutralImage = neutralImage3;
                stressedImage = stressedImage3;
                deadImage = deadImage3;

                portrait.sprite = getCurrentMood();
                levelCounter++;
            }
        }
        else
        {
            interact(CritterInteraction.ignore);
            healthDTime = 10f;
        }
    }
}
